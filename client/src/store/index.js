import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    chatMessages: [],
    clients: [],
    socketError: "",
  },
  getters: {
    chatMessages(state) {
      return state.chatMessages;
    },
    clients(state) {
      return state.clients;
    },
    socketError(state) {
      return state.socketError;
    },
  },
  mutations: {
    addChatMessage(state, message) {
      state.chatMessages.push(message);
    },
    setClients(state, clients) {
      state.clients = clients;
    },
    setSocketError(state, error) {
      state.socketError = error;
    },
  },
  actions: {},
  modules: {},
});
