const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const { instrument } = require("@socket.io/admin-ui");
const io = new Server(server,{
  allowEIO3: true, // false by default
  cors: {
    origin: ["http://localhost:8080","https://admin.socket.io"],
    credentials: true
  }
});



io.on('connection', (socket) => {
  let users = [];

  for (let [id, socket] of io.of("/").sockets) {
    users.push({
      userID: id,
      username: socket.username,
    });
  }

  socket.emit("clients", users);

  
  socket.on("login", (username)=>{
    if (users.find(user => user.username === username)){
      socket.emit("error", `${username} is already taken`);
    }
    else {
    socket.username = username;
    let newUsers = [];
    for (let [id, socket] of io.of("/").sockets) {
      newUsers.push({
        userID: id,
        username: socket.username,
      });
    }
    io.emit("clients", newUsers);
    io.emit("clientChat", `${socket.username} has entered the chat`);
    }
  });

  socket.on("sendChat", (message) => {
    console.log(message)
    io.emit("clientChat", `${socket.username}: ${message}`);
  });

  socket.on('disconnect', function(){
    if (socket.username) {
      io.emit("clientChat", `${socket.username} has left the chat`);
      users = users.filter(user => user.userID !== socket.id)
      let newUsers = [];
      for (let [id, socket] of io.of("/").sockets) {
        newUsers.push({
          userID: id,
          username: socket.username,
        });
      }
      io.emit("clients", newUsers);
    }
  });
});

instrument(io, {
  auth: false
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});